require.config({
  baseUrl: "./libs", //Path relative to bootstrapping html file.
  paths: {   //Paths relative baseUrl, only those that deviate from baseUrl/{modulename} are explicitly listed.
    "entryscape-catalog-embed": "..",
    "requireLib": "requirejs/require",
    "mustache": "mustache/mustache.min",
    "text": "requirejs-text/text",
  },
  packages: [ //Config defined using packages to allow for main.js when requiring just config.
    {
      name: "config",
      location: "../config",
      main: "main"
    }
  ],
  namespace: "entryscape",
  module: [
      {
          name: "entryscape",
          include: ["requireLib", "entryscape-catalog-embed/init"],
          create: true
      }
  ],
  deps: [
    "entryscape-catalog-embed/init"
  ]
});
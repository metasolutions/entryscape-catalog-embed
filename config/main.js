define([
  "config/local",
  "entryscape-catalog-embed/merge"
], function(local, merge) {
  var pageconfig = typeof __entryscape_config === "undefined" ? {} : __entryscape_config;
  var ST_EDP = "https://static.entryscape.com/mixed/edp/";
  return merge({
    loadingMessage: {
      "en": "Loading list of datasets",
      "sv": "Laddar lista med datamängder"
    },
    emptyMessage: {
      "en": "At the moment there are no datasets to show.",
      "sv": "För närvarande finns det inga datamängder att visa."
    },
    errorMessage: {
      "en": "A technical error causes the list of datasets to be unavailable at this moment. We are working on solving the problem.",
      "sv": "Ett tekniskt fel gör att listan med datamängder inte kan visas just nu. Vi jobbar med att lösa problemet."
    },
    "defaultThemeTitle": {
      "en": "The dataset is not categorized.",
      "sv": "Datamängden är inte kategoriserad."
    },
    "themeTitles": {
      "AGRI": {
        "en": "Agriculture, fisheries, forestry and food",
        "sv": "Jordbruk, fiske, skogsbruk och livsmedel",
        "nb": "Jordbruk, fiskeri, skogbruk og mat",
        "da": "Landbrug, fiskeri, skovbrug og mad",
        "de": "Landwirtschaft, Fischerei, Forstwirtschaft und Lebensmittel"
      },
      "ECON": {
        "en": "Economy and finance",
        "sv": "Ekonomi och finans",
        "nb": "Økonomi og finans",
        "da": "Økonomi og finans",
        "de": "Wirtschaft und Finanzen"
      },
      "EDUC": {
        "en": "Education, culture and sport",
        "sv": "Utbildning, kultur och sport",
        "nb": "Utdanning, kultur og sport",
        "da": "Uddannelse, kultur og sport",
        "de": "Bildung, Kultur und Sport"
      },
      "ENER": {
        "en": "Energy",
        "sv": "Energi",
        "nb": "Energi",
        "da": "Energi",
        "de": "Energie"
      },
      "ENVI": {
        "en": "Environment",
        "sv": "Miljö",
        "nb": "Miljø",
        "da": "Miljø",
        "de": "Umwelt"
      },
      "GOVE": {
        "en": "Government and public sector",
        "sv": "Regeringen och den offentliga sektorn",
        "nb": "Forvaltning og offentlig sektor",
        "da": "Forvaltning og offentlig sektor",
        "de": "Regierung und öffentlicher Sektor"
      },
      "HEAL": {
        "en": "Health",
        "sv": "Hälsa",
        "nb": "Helse",
        "da": "Helbred",
        "de": "Gesundheit"
      },
      "INTR": {
        "en": "International issues",
        "sv": "Internationella frågor",
        "nb": "Internasjonale temaer",
        "da": "Internationale forhold",
        "de": "Internationale Angelegenheiten"
      },
      "JUST": {
        "en": "Justice, legal system and public safety",
        "sv": "Rättvisa, rättsliga system och allmän säkerhet",
        "nb": "Justis, rettssystem og allmenn sikkerhet",
        "da": "Justits, rettssystem og almen sikkerhed",
        "de": "Gerechtigkeit, Rechtsordnung und öffentliche Sicherheit"
      },
      "SOCI": {
        "en": "Population and society",
        "sv": "Befolkning och samhälle",
        "nb": "Befolkning og samfunn",
        "da": "Befolkning og samfund",
        "de": "Bevölkerung und Gesellschaft"
      },
      "REGI": {
        "en": "Regions and cities",
        "sv": "Regioner och städer",
        "nb": "Regioner og byer",
        "da": "Regioner og byer",
        "de": "Regionen und Städte"
      },
      "TECH": {
        "en": "Science and technology",
        "sv": "Vetenskap och teknik",
        "nb": "Vitenskap og teknologi",
        "da": "Videnskab og teknologi",
        "de": "Wissenschaft und Technik"
      },
      "TRAN": {
        "en": "Transport",
        "sv": "Transport",
        "nb": "Transport",
        "da": "Transport",
        "de": "Transport"
      }
    },
    loadingTemplate: "<div class='noDatasets'>{{message}}</div>",
    emptyTemplate: "<div class='noDatasets'>{{message}}</div>",
    errorTemplate: "<div class='noDatasets'>{{message}}</div>",
    imageTemplate_FA: "<i class='fa fa-{{image}} fa-3x fa-lg'></i>",
    imageTemplate_IMG: "<img src='{{{imageURL}}}'>",
    template: "<div class='dataset {{count}}'>" +
    "<div class='datasetImage' title='{{theme}}'>{{{imageBlock}}}</div>" +
    "<div class='datasetInfo'><a class='heavylink' href='{{{detailedViewURL}}}' {{{target}}}>{{title}}</a>" +
    "<span>{{{description}}}</span></div></div>",
    themeImageWhiteBackground: false,
    "themeStyle": "IMG", //May be "IMG" or "FA".
    themeImageDefault_IMG: ST_EDP + "ANY.png",
    themeImageDefault_FA: "database",
    "themeImages_IMG": { //This is the default European data portal theme images.
      "AGRI": ST_EDP + "AGRI.png",
      "ECON": ST_EDP + "ECON.png",
      "EDUC": ST_EDP + "EDUC.png",
      "ENER": ST_EDP + "ENER.png",
      "ENVI": ST_EDP + "ENVI.png",
      "GOVE": ST_EDP + "GOVE.png",
      "HEAL": ST_EDP + "HEAL.png",
      "INTR": ST_EDP + "INTR.png",
      "JUST": ST_EDP + "JUST.png",
      "SOCI": ST_EDP + "SOCI.png",
      "REGI": ST_EDP + "REGI.png",
      "TECH": ST_EDP + "TECH.png",
      "TRAN": ST_EDP + "TRAN.png"
    },
    "themeImages_FA": {
      "AGRI": "tree",
      "ECON": "area-chart",
      "EDUC": "graduation-cap",
      "ENER": "lightbulb-o",
      "ENVI": "recycle",
      "GOVE": "university",
      "HEAL": "heartbeat",
      "INTR": "globe",
      "JUST": "balance-scale",
      "SOCI": "users",
      "REGI": "building",
      "TECH": "rocket",
      "TRAN": "subway"
    },
    onlyPublished: true, //Unused currently, needs to be supported by recursive flag in EntryStore
    storeExt: "/store/",
    previewExt: "/#view=dataset&resource={{{resourceURL}}}",
    target: "", //May be "_blank"
    FACSS: "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css",
    ignoreFACSS: false, //Only relevant if themeStyle: "FA"
    customCSS: false
  }, local, pageconfig);
});

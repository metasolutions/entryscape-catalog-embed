define([], function() {
    /**
     * Deep merge of two objects or concatenation of arrays.
     * If two objects are given the latter values are merged into te first object.
     * If there are subobjects the procedure are repeated for those.
     * If a key is prefixed with a ! the value is overwritten rather than merged.
     */
    var merge = function(o1, o2) {
        if (o1 instanceof Array && o2 instanceof Array) {
            return o1.concat(o2);
        } else if (typeof o1 === "object" &&
            typeof o2 === "object" &&
                o1 !== null &&
                o2 !== null) {
            for (key2 in o2) if (o2.hasOwnProperty(key2)) {
                if (key2[0] === "!") {
                    key2 = key2.substr(1);
                    o1[key2] = o2["!"+key2];
                } else if (!o1.hasOwnProperty(key2)) {
                    o1[key2] = o2[key2];
                } else {
                    o1[key2] = merge(o1[key2], o2[key2]);
                }
            }
            return o1;
        } else {
            return o2;
        }
    };

    return function() {
        var args = Array.prototype.slice.call(arguments), i = 1, l = args.length, o = args[0];
        for (;i<l;i++) {
            o = merge(o,args[i]);
        }
        return o;
    };
});
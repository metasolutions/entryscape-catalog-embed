var scripts =  document.querySelectorAll('script[data-repository]'),
  scriptNode = scripts[0];

define([
  "rdfjson/Graph",
  "rdfjson/namespaces",
  "mustache",
  "config",
  "text!./style.css",
  "./docReady"
], function (Graph, namespaces, mustache, config, styleString, docReady) {

  //Just wait for the dom
  docReady(function () {
    namespaces.add("dcat", "http://www.w3.org/ns/dcat#");

    var navlang;
      if(typeof navigator != "undefined"){
          // Default locale for browsers (ensure it's read from user-settings not download locale).
          navlang = (navigator.languages && navigator.languages.length) ?
              navigator.languages[0] : (navigator.language || navigator.userLanguage);
          if(navlang){
              navlang = navlang.toLowerCase();
          }
      }

    var elementId, language, entryId, contextId, repository, themeStyle, target, onlyPublished, ignoreFACSS, customCSS, previewExt, storeExt;

    //Find the script tag and extract config from data attributes.
    elementId = scriptNode.getAttribute("data-element-id");
    language = scriptNode.getAttribute("data-language-code");
    entryId = scriptNode.getAttribute("data-entry-id");
    contextId = scriptNode.getAttribute("data-context-id");
    repository = scriptNode.getAttribute("data-repository");
    themeStyle = scriptNode.getAttribute("data-theme-style");
    target = scriptNode.getAttribute("data-target");
    onlyPublished = scriptNode.getAttribute("data-only-published");
    ignoreFACSS = scriptNode.getAttribute("data-ignore-fa-css");
    customCSS = scriptNode.getAttribute("data-custom-css");
    storeExt = scriptNode.getAttribute("data-store-ext");
    previewExt = scriptNode.getAttribute("data-preview-ext");


    //Combine parameters from script with config
    elementId = elementId || config.elementId || "dcat";
    language = language || config.language || navlang || "en";
    entryId = entryId || config.entryId;
    contextId = contextId || config.contextId;
    repository = repository || config.repository;
    themeStyle = themeStyle || config.themeStyle;
    target = target || config.target;
    onlyPublished = onlyPublished != null ? onlyPublished === "true" : config.onlyPublished;
    ignoreFACSS = ignoreFACSS != null ? true : config.ignoreFACSS;
    customCSS = customCSS != null ? true: config.customCSS;
    storeExt = storeExt || config.storeExt;
    previewExt = previewExt || config.previewExt;

    //Helper localize function for extracting correct translation of messages.
    var localize = function(bundle) {
      return bundle[language] || bundle.en || bundle.sv;
    };

    //Helper to insert CSS
    var css = function(href) {
      var link = document.createElement("link");
      link.href = href;
      link.type = "text/css";
      link.rel = "stylesheet";
      document.getElementsByTagName("head")[0].appendChild(link);
    };
    //Helper to insert CSS from text
    var cssTxt = function(cssStr) {
      var style = document.createElement('style');
      style.type= 'text/css';
      style.innerHTML = cssStr;
      document.getElementsByTagName('head')[0].appendChild(style);
    };

    //Find the node where to insert the datasets.
    var node = document.getElementById(elementId);
    //If no node give, create one just after the script tag.
    if (node == null && scriptNode != null) {
      node = document.createElement('div');
      scriptNode.parentNode.insertBefore(node, scriptNode.nextSibling);
    }
    //Insert Font-awesome css if needed and not explicitly excluded (if exists in surrounding page)
    if (themeStyle === "FA" && !ignoreFACSS) {
      css(config.FACSS);
    }
    if (!customCSS) {
      cssTxt(styleString);
    }

    //If theme images are not transparent, use this setting.
    if (config.themeImageWhiteBackground) {
      node.className = node.className + " whiteBackgroundOnThemeImage";
    }
    //Set a default loading message.
    node.innerHTML = mustache.render(config.loadingTemplate,
      {message: localize(config.loadingMessage)});

    //Switch to empty message after a timeout
    //If datasets are loaded before the timeout, the timeout will be canceled.
    var loadError = setTimeout(function () {
      node.innerHTML = mustache.render(config.errorTemplate,
        {message: localize(config.errorMessage)});
    }, 5000);

    //Switch to empty message after a timeout
    // If datasets are loaded before the timeout, the timeout will be canceled.
    var noDatasets = function () {
        node.innerHTML = mustache.render(config.emptyTemplate,
            {message: localize(config.emptyMessage)});
    };

    var getDetailedViewURL = function(resourceURL) {
      return repository+mustache.render(previewExt, {resourceURL: resourceURL});
    };

    const mdrthemebase = "http://publications.europa.eu/resource/authority/data-theme/";
    window.dcat = function (data) {
      var g = new Graph(data);
      var stmts = g.find(null, "rdf:type", "dcat:Dataset");
      var htmlArr = [];
      clearTimeout(loadError);
      if (stmts.length > 0) {
        var views = [];
        for (var i = 0; i < stmts.length; i++) {
          var stmt = stmts[i];
          var desc = g.findFirstValue(stmt.getSubject(), "dcterms:description");
          desc = desc.replace("<", "&lt;").replace(">", "&gt;").replace(/(\r\n|\r|\n)/g, "<br/>");
          var title=null;
          if (language) {
            var t_flat = null, t_lang = null, t_any = null;
              var stmtst = g.find(stmt.getSubject(), "dcterms:title");
              for (var k=0;k<stmtst.length;k++) {
                var v = stmtst[k].getValue();
                var l = stmtst[k].getLanguage();
                if (l) {
                    if (l.indexOf(language) === 0) {
                        t_lang = v;
                    } else {
                        t_any = v;
                    }
                } else {
                    t_flat = v;
                }
              }
              title = t_lang || t_any || t_flat;
          } else {
            title = g.findFirstValue(stmt.getSubject(), "dcterms:title");
          }
          var view = {
            title: title || "No title specified",
            description: desc,
            detailedViewURL: getDetailedViewURL(stmt.getSubject()),
            target: target !== "" ? "target='"+target+"'" : ""
          };
          var theme = g.findFirstValue(stmt.getSubject(), "dcat:theme");
          if (theme == null || theme.indexOf(mdrthemebase) !== 0) {
            view.theme = localize(config.defaultThemeTitle);
            if (themeStyle === "IMG") {
              view.imageBlock = mustache.render(config.imageTemplate_IMG, {imageURL: config.themeImageDefault_IMG});
            } else {
              view.imageBlock = mustache.render(config.imageTemplate_FA, {image: config.themeImageDefault_FA});
            }
          } else {
            theme = theme.substr(60);
            view.theme = localize(config.themeTitles[theme]);
            if (themeStyle === "IMG") {
              view.imageBlock = mustache.render(config.imageTemplate_IMG, {imageURL: config.themeImages_IMG[theme]});
            } else {
              view.imageBlock = mustache.render(config.imageTemplate_FA, {image: config.themeImages_FA[theme]});
            }
          }
          views.push(view);
        }
        views.sort(function(v1, v2) {
            if (v1.title < v2.title) {
              return -1;
            } else if (v1.title > v2.title) {
              return 1;
            } else {
              return 0;
            }
        });
        for (var j=0;j<views.length;j++) {
          views[j].count = j % 2 === 0 ? "even" : "odd";
          htmlArr.push(mustache.render(config.template, views[j]));
        }
        node.innerHTML = htmlArr.join("");
      } else {
        noDatasets();
      }
    };

    var extractBase = function () {
      return repository + storeExt + contextId + "/metadata/" + entryId + "?format=application/json&recursive=dcat&callback=dcat&ignoreAuth";
    };

    var script = document.createElement('script');
    script.src = extractBase();
    document.getElementsByTagName('head')[0].appendChild(script);
  });
});

# Embed of datasets from a single catalog via script

This repository allows the embedding of dataset lists on other webpages via the use of a javascript and some css.

## Installing

1. Make sure you have node, npm and bower installed.
2. Run bower install.
3. Provide config/local.js (*)
4. Build by executing: cd build;./build.sh

(*) In the most simple case just copy config/local.js_example. If you need to hardcode certain configuration options take a look at config/local.js_options

## Looking at examples

Assuming you are serving the repository at localhost:8080/embed and that you have configured and built the repository successfully you can check a realistic embedding example in a bootstrap driven site at:

    http://localhost:8080/embed/samples
    
For a cleaner example look at:

    http://localhost:8080/embed/samples/script_config.html
    
This one assumes you have created a samples/config.js, possibly based on the samples/config.js_example.

Also, it is possible to provide the configuration directly as attributes in the script tag, see how it is done in:

    http://localhost:8080/embed/samples/script_attributes.html

Check out the config/local.js_options for which attribute names to use for different configuration options.